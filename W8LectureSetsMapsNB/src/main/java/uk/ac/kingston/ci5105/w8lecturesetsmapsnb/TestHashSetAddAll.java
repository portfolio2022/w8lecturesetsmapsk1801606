package uk.ac.kingston.ci5105.w8lecturesetsmapsnb;

import java.util.Collection;
import java.util.HashSet;
import java.util.ArrayList;

public class TestHashSetAddAll {
  public static void main(String[] args) {
    // Create a HashSet
    Collection<String> set = new HashSet<>();
    Collection<String> list = new ArrayList<>();

    list.add("Alexander"); list.add("Boris");
    list.add("de Pfeffel"); list.add("Johnson");
    list.add("Boris");

    System.out.println("Elements in list: " + list);
    // LOOKY HERE
    set.addAll(list);
    System.out.println("# of elements in list: " + list.size());
    System.out.println("# of elements in set: " + set.size());

    list.clear();
    list.addAll(set); // list.addAll(set); // do it again!!

    System.out.print("Names in list, i replacing e are: ");
    for (String item: list)
      System.out.print(item.replaceAll("e", "i") + " ");
  }
}