package uk.ac.kingston.ci5105.w8lecturesetsmapsnb;
import java.util.Arrays;

public class TestHashCode {
  public static void main(String[] args) {
    String aBarryString = "Barry";
    String anotherBarryString = "Barry";
    // LOOKY HERE
    System.out.println("Printing Barry's hashcode: " + aBarryString.hashCode() + " and the other Barry's hashcode: "
        + anotherBarryString.hashCode());
    System.out
        .println("Checking if Barry is .equal() " + "to anotherBarry: " + aBarryString.equals(anotherBarryString));

    int[] intArr = { 5, 7, 9 };
    int[] intArr2 = { 5, 7, 9 };
    System.out
        .println("Printing intArr's hashcode: " + intArr.hashCode() + " and intArr2's hashcode: " + intArr2.hashCode());
    System.out.println("Printing hashcodes via Arrays.hashCode(), " + "intArr's  " + Arrays.hashCode(intArr)
        + " and intArr2's: " + Arrays.hashCode(intArr2));

    System.out.println("Checking if intArr is .equal() " + "to intArr2: " + intArr.equals(intArr2));
    System.out.println("Checking if intArr is Arrays.equal() " + "to intArr2: " + Arrays.equals(intArr, intArr2));
  }
}