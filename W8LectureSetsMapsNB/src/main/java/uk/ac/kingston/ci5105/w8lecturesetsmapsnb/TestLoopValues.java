package uk.ac.kingston.ci5105.w8lecturesetsmapsnb;

import java.util.Map;
import java.util.HashMap;

public class TestLoopValues {
    public static void main(String [] args) {
        Map<Integer,String> hashMap = new HashMap();
        hashMap.put(4, "Johnson");
        hashMap.put(2, "Boris");
        hashMap.put(1, "Alexander");
        hashMap.put(3, "de Pfeifel");
        System.out.println("HashMapElements");
        System.out.println(hashMap);
        // LOOKY HERE
        for (String value: hashMap.values()){
            System.out.println("Value: " + value);
        }  
    }
}