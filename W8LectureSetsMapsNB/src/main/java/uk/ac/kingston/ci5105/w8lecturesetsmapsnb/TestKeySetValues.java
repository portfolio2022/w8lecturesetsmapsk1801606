package uk.ac.kingston.ci5105.w8lecturesetsmapsnb;
import java.util.Collection;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;

public class TestKeySetValues {
    public static void main(String [] args) {

        Map<Integer,String> hashMap = new HashMap();
        //Insert objects in the HashMap
        hashMap.put(4, "Johnson");
        hashMap.put(2, "Boris");
        hashMap.put(1, "Alexander");
        hashMap.put(3, "de Pfeifel");
        //Print the HashMap Object
        System.out.println("HashMapElements");
        System.out.println(hashMap);
        //Get all the keys
        Set<Integer> keys = hashMap.keySet();
        System.out.println("Here is the key set: " + keys);
        // Get all the values
        Collection<String> valuesCollection = hashMap.values();
        System.out.println("Here is the values Collection: " + valuesCollection);

    }
}