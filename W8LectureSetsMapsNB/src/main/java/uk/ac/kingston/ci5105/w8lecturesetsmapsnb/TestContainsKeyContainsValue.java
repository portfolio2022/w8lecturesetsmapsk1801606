package uk.ac.kingston.ci5105.w8lecturesetsmapsnb;
import java.util.Map;
import java.util.HashMap;

public class TestContainsKeyContainsValue{
    public static void main(String [] args) {

        Map<Integer,String> hashMap = new HashMap();
        //Insert objects in the HashMap
        hashMap.put(4, "Johnson");
        hashMap.put(2, "Boris");
        hashMap.put(1, "Alexander");
        hashMap.put(3, "de Pfeifel");
        //Print the HashMap Object
        System.out.println("HashMapElements");
        System.out.println(hashMap);
        if(hashMap.containsKey(4)){
            System.out.println("The hashMap contains the key 4");
        }
        if(hashMap.containsValue("Boris")){
            System.out.println("The hashMap contains the value Boris");
        }
       

    }
}