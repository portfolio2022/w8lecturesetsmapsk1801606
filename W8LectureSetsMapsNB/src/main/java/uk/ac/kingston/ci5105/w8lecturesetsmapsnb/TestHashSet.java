/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package uk.ac.kingston.ci5105.w8lecturesetsmapsnb;

import java.util.Collection;
import java.util.HashSet;

/**
 *
 * @author dlivi
 */
public class TestHashSet {
     public static void main(String [] args) {

        Collection<String> set = new HashSet();
        
        //add objects in the HashSet
         set.add("Alexander");
         set.add("Boris");
         set.add("de Pfeifel");
         set.add("Joohnson");
         set.add("Boris");
         System.out.println("Elements in set:" + set);
         System.out.println("Number of elements in set:" + set.size());
         System.out.println("Is Smith in set?" + set.contains("Smith"));
         System.out.println("Is Boris in set?" + set.contains("Boris"));
         set.remove("Boris");
         System.out.println("Names in set in upper case are");
         for (String s: set){
             System.out.print(s.toUpperCase() + " ");
         }
         set.clear();
         System.out.println("Elements in set:" + set);
         
    }
}
