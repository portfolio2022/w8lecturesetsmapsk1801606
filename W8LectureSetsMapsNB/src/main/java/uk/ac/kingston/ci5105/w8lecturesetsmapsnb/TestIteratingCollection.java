package uk.ac.kingston.ci5105.w8lecturesetsmapsnb;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator; // LOOKY HERE

public class TestIteratingCollection {
  public static void main(String[] args) {
    Collection<String> set = new HashSet<>();
    set.add("Alexander"); set.add("Boris");
    set.add("de Pfeffel"); set.add("Johnson");
    set.add("Boris");

    System.out.println("Elements in set: " + set);
    // LOOKY HERE
    String[] setArr = set.toArray(new String[0]);
    for (int i = 0; i < setArr.length; i++) {
      System.out.print(setArr[i].toUpperCase() + " ");
    }
    System.out.println();
    for (String item: set)
      System.out.print(item.replaceAll("e", "i") + " ");
    System.out.println();
    Iterator<String> iter = set.iterator();
    while (iter.hasNext()) {
      System.out.print(iter.next().toLowerCase() + " ");
    }
  }
}
