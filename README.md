# W8LectureSetsMaps

This is the sample code for [lecture 8 sets and maps](https://kingston.app.box.com/file/1065537864188?s=29zzaln3x8v142rkfjplpvnm5ewy7f4z) It is in a NetBeans project called 'W8LectureSetsMapsNB' . 

In the workshop we will go over how you can clone this project and run the sample code. The associated workshop has a couple of exercises on the .equals() and .hashcode() concept discussed in the first part of the lecture. These are methods that you override from the (Java Object class)  [https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Object.html]

To work on starting code in a repository in your own sub-group you will create a copy of this project. However, until your access has been confirmed your sub-group has not been created and so you will only be able to clone and work locally by doing the following:

- [ ]  [Create a project access token ](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-tokens). This will be used to allow you to authenticate to GitLab.com/KUGitlab for the purposes of typical operations e.g. cloning and pushing to private repositories in your sub-group.  You will typically only need on e personal access token but you can create a new one whenever you require it and delete old ones. 
- [ ]  [Clone this new Repository using your Personal Access Token](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-using-a-token). You can do this from an IDE (e.g. NetBeans) or from the command-line (e.g. Git Bash) 
- [ ] Use a Git development process (add, commit, push) to version manage your code.  This will be covered in the module but is [typically](https://docs.gitlab.com/ee/gitlab-basics/add-file.html)



***


